#!/bin/bash -ex

curl -X POST https://api.heroku.com/apps -H "Accept: application/vnd.heroku+json; version=3" -H "Authorization: Bearer $HEROKU_API_KEY" -H "Content-Type: application/json" -d "{\"name\":\"$PROJECT_NAME-$CI_COMMIT_REF_SLUG\",\"region\":\"eu\"}"
apt-get update -qy
apt-get install -y ruby-dev
gem install dpl
dpl --provider=heroku --app=$PROJECT_NAME-$CI_COMMIT_REF_SLUG --api-key=$HEROKU_API_KEY
